;;; init.el --- emacs config files -*- lexical-binding: t; -*-
;; Copyright (C) 2023 Darylle PV

(defvar efs/default-theme 'modus-vivendi-deuteranopia)

; For monospace, I used my own Dat PP, modified version of Iosevka SS08.
(defvar efs/monospace-font "Cascadia Mono")
(defvar efs/sans-serif-font "SF Pro")
(defvar efs/default-font-size 100)
(defvar efs/default-fixed-font-size 100)
(defvar efs/default-variable-font-size 100)
(defvar efs/org-fixed-font-size 140)
(defvar efs/org-variable-font-size 195)

; Default Cache directory
(defvar efs/emacs-cache-dir "~/.cache/emacs")
; Default Org files storage
(defvar efs/org-dir "~/Documents/org")

(defun efs/turn-on-autopairs ()
  "Turn on auto-pairs mode."
  (electric-pair-mode t))

(defun efs/turn-on-visual-line ()
  "Turn on visual line mode."
  (visual-line-mode 1))

(defun efs/shrink-win (n)
  "Shrink window size n times."
  (while (> n 0)
    (shrink-window 1)
    (setq n (- n 1))))

(defun efs/add-column-ruler (col-num)
  (setq fill-column col-num)
  (display-fill-column-indicator-mode))

(require 'package)
; Package sources
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org"   . "https://orgmode.org/elpa/")
                         ("elpa"  . "https://elpa.gnu.org/packages/")))
(package-initialize)
(unless package-archive-contents (package-refresh-contents))
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)
(setq use-package-always-ensure t)
(use-package quelpa)

(use-package no-littering)
(setq user-emacs-directory efs/emacs-cache-dir)
(setq auto-save-file-name-transforms
      `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))
; store customizations in cache/custom.el instead of init.el
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
; native compilation cache
(when (fboundp 'startup-redirect-eln-cache)
  (startup-redirect-eln-cache
    (convert-standard-filename
      (expand-file-name "var/eln-cache/" user-emacs-directory))))

(use-package recentf)
(add-to-list 'recentf-exclude
             (recentf-expand-file-name no-littering-var-directory))

; Make backup files in one dir
(setq backup-directory-alist `(("." . "~/.cache/emacs/bak")))

(menu-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(scroll-bar-mode -1)
(set-fringe-mode 0)
(setq split-height-threshold nil)
(setq split-width-threshold 0)

; Substitute some words with corresponding symbols (e.g. lambda, epsilon)
(defun pretty-lambda ()
  "Make some word or string show as pretty Unicode symbols."
  (setq prettify-symbols-alist
        '(("lambda" . 955))))
(add-hook 'emacs-lisp-mode-hook 'pretty-lambda)
(add-hook 'scheme-mode-hook 'pretty-lambda)
(global-prettify-symbols-mode 1)

(use-package nerd-icons)
(use-package all-the-icons)

(use-package doom-modeline
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 15))
  :config
  (setq nerd-icons-color-icons nil
        doom-modeline-icon nil))

(setq-default
 inhibit-startup-message t
 inhibit-startup-screen t
 initial-scratch-message nil)

(add-to-list 'default-frame-alist '(fullscreen . maximized))
; Fix window maximize not taking the full screen
(setq frame-resize-pixelwise t)

(use-package ef-themes
  :config
  (setq ef-themes-to-toggle
        '(ef-deuteranopia-light
          ef-elea-dark)))

(use-package doom-themes
  :config
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))

(use-package kaolin-themes
  :config (kaolin-treemacs-theme))

(use-package modus-themes
  :custom
  (modus-themes-italic-constructs nil)
  (modus-themes-bold-constructs t)
  (modus-themes-mixed-fonts t)
  (modus-themes-variable-pitch-ui nil)
  (modus-themes-disable-other-themes t)
  (modus-themes-org-blocks 'tinted-background)
  (modus-themes-headings
   '((t . (rainbow))))
  (modus-themes-common-palette-overrides
   '((fringe unspecified)
     (underline-link unspecified)
     (underline-link-visited unspecified)
     (underline-link-symbolic unspecified)
     (border-mode-line-inactive unspecified)))
  (modus-operandi-deuteranopia-palette-overrides
   '((bg-main "#F5FFFF")
     (keyword "#1B10E9"))))

(use-package darktooth-theme)

; custom theme dir
(add-to-list 'custom-theme-load-path "~/.config/emacs/themes")

; zenburn settings
(setq zenburn-use-variable-pitch t)
(setq zenburn-scale-org-headlines t)

(use-package heaven-and-hell
  :config
  (setq heaven-and-hell-themes
        '((dark .  darktooth-dark)
          (light . ef-summer)))
  (setq heaven-and-hell-load-theme-no-confirm t))

; Disable other themes first
(mapc #'disable-theme custom-enabled-themes)

; Load theme
(load-theme efs/default-theme t)

(defun efs/set-font-faces ()
  "Set fonts."
  (set-face-attribute 'default nil
                       :font efs/monospace-font
                       :height efs/default-font-size)

  (set-face-attribute 'fixed-pitch nil
                      :font efs/monospace-font
                      :height efs/default-fixed-font-size)

  (set-face-attribute 'variable-pitch nil
                      :font efs/sans-serif-font
                      :height efs/default-variable-font-size
                      :weight 'regular)
  (set-fontset-font t 'phonetic (font-spec :family "DejaVu Sans Mono"))

  (dolist (char (string-to-list "æθðŋʷʸˈˌ"))
    (set-fontset-font nil char (font-spec :family "DejaVu Sans Mono")))

  (set-fontset-font t 'phonetic (font-spec :family "DejaVu Sans Mono")))

(if
  (daemonp)
  (add-hook 'after-make-frame-functions
            (lambda (frame)
              (with-selected-frame frame (efs/set-font-faces))))
  (efs/set-font-faces))

(setq ef-themes-mixed-fonts t
      ef-themes-variable-pitch-ui nil)

(use-package evil
  :init
  (setq evil-want-integration t
        evil-want-keybinding nil
        evil-want-C-u-scroll t
        evil-want-C-i-jump nil)
  :config
  (evil-mode 1)
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)
  (evil-set-initial-state 'message-buffer-mode 'normal)
  (evil-set-undo-system 'undo-redo))

(use-package evil-org
  :after org
  :config
  (add-hook 'org-mode-hook 'evil-org-mode)
  (add-hook 'evil-org-mode-hook
            (lambda () (evil-org-set-key-theme)))
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package dired
  :ensure nil
  :commands (dired dired-jump)
  :bind (("C-x C-j" . dired-jump))
  :custom ((dired-listing-switches "-agho --group-directories-first"))
  :config
  (evil-collection-define-key 'normal 'dired-mode-map
    "h" 'dired-single-up-directory
    "l" 'dired-single-buffer))
(use-package all-the-icons-dired
  :hook (dired-mode . all-the-icons-dired-mode))

(use-package dired-single :commands (dired dired-jump))
; Fix dired opening multiple buffers every file/folder opened
(setf dired-kill-when-opening-new-dired-buffer t)

(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom ((projectile-completion-system 'ivy))
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  (when (file-directory-p "~/Documents/dev")
    (setq projectile-project-search-path '("~/Documents/dev")))
  (setq projectile-switch-project-action #'projectile-dired))

; Integrate Ivy UI for browsing projects
(use-package counsel-projectile
  :after projectile
  :config (counsel-projectile-mode))

(use-package ivy
  :diminish
  :bind (("C-s" . swiper)
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)
         ("C-n" . ivy-next-line)
         ("C-p" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-p" . ivy-previous-line)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-p" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :config
  (setq ivy-use-selectable-prompt t)
  (ivy-mode 1))

; Friendly interface for Ivy
(use-package ivy-rich
  :after ivy
  :init
  (ivy-rich-mode 1))

; For sorting filtering
(use-package ivy-prescient
  :after counsel
  :custom
  (ivy-prescient-enable-filtering nil)
  :config
  (ivy-prescient-mode 1))

(use-package helm)

(use-package counsel
  :bind (("C-M-b" . 'counsel-switch-buffer))
  :custom
  (counsel-linux-app-format-function #'counsel-linux-app-format-function-name-only)
  :config
  (counsel-mode 1))

(use-package company
  :hook (lsp-mode . company-mode)
  :bind
  (:map company-active-map ("<tab>" . company-complete-selection))
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0))

; Company frontend with icons
(use-package company-box
  :hook (company-mode . company-box-mode))

; enable completion in all buffers
(add-hook 'after-init-hook 'global-company-mode)
;except org mode
(defun efs/turn-off-completion () (company-mode -1))

(use-package yasnippet :config (yas-global-mode))
(use-package yasnippet-snippets)

(set-language-environment 'utf-8)
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-selection-coding-system 'utf-8)
; Enable line numbers in source code files only
(use-package display-line-numbers
  :ensure nil
  :hook ((prog-mode conf-mode) . display-line-numbers-mode))

;; (electric-pair-mode t)             ; autopairs
(savehist-mode t)                  ; save minibuffer history
(save-place-mode t)                ; jump to the last known position when reopening a file
(show-paren-mode t)                ; visualization of matching parens
(global-hl-line-mode t)            ; line highlighting
(global-auto-revert-mode 1)        ; automatically reload externally modified files
(fset 'yes-or-no-p 'y-or-n-p)      ; replace "yes/no" prompts with "y/n"
(setq-default truncate-lines t)    ; unwrap by default

(setq-default
 indent-tabs-mode nil  ; tab with spaces
 indent-line-function 'insert-tab
 tab-width 4
 fill-column 78
 cursor-type 'bar
 column-number-mode t
 vc-follow-symlinks t
 major-mode 'text-mode
 ring-bell-function 'ignore
 cursor-in-non-selected-windows nil
 inhibit-compacting-font-caches nil)

(use-package rg)

(use-package files
  :ensure nil
  :custom
  (backup-directory-alist `(("." . ,(concat user-emacs-directory "backup"))))
  (backup-by-copying t)               ; Always use copying to create backup files
  (delete-old-versions t)             ; Delete excess backup versions
  (kept-new-versions 6)               ; Number of newest versions to keep when a new backup is made
  (kept-old-versions 2)               ; Number of oldest versions to keep when a new backup is made
  (version-control t)                 ; Make numeric backup versions unconditionally
  (auto-save-default nil)             ; Stop creating #autosave# files
  (delete-by-moving-to-trash t)       ; Move deleted files to the trash
  (mode-require-final-newline nil)    ; Don't add newlines at the end of files
  (large-file-warning-threshold nil)) ; Open large files without requesting confirmation

(use-package highlight-indent-guides
  :hook
  (prog-mode . highlight-indent-guides-mode)
  :config
  (setq highlight-indent-guides-responsive 'top)
  (setq highlight-indent-guides-method 'character)
  (setq highlight-indent-guides-suppress-auto-error t)
  (setq highlight-indent-guides-auto-enabled nil)
  (set-face-background 'highlight-indent-guides-odd-face "darkgray")
  (set-face-background 'highlight-indent-guides-even-face "dimgray")
  (set-face-foreground 'highlight-indent-guides-character-face "dimgray"))

(use-package hl-todo
       :custom-face
       (hl-todo ((t (:inherit hl-todo :italic t))))
       :hook ((prog-mode . hl-todo-mode)
              (latex-mode . hl-todo-mode)))
(setq hl-todo-keyword-faces
      `(("TODO"   warning bold)
        ("FIXME"  error bold)
        ("DEBUG"  warning bold)
        ("NOTE"   success bold)))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(defun efs/visual-fill-mode ()
  "Add margins to shrink view for org-mode etc."
  (setq visual-fill-column-width 100
   visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook
  (org-mode . efs/visual-fill-mode)
  (markdown-mode . efs/visual-fill-mode))

(use-package python-mode
  :hook
  (efs/turn-on-autopairs . python-mode)
  (display-fill-column-indicator-mode . python-mode)
  :config
  (setq python-indent-offset 4))

(use-package pyvenv
  :after python-mode
  :config
  (pyvenv-mode 1))

(defun efs/web-mode-setup ()
  "Web-related file settings."
  (setq web-mode-attr-indent-offset 2
        web-mode-enable-css-colorization t
        web-mode-enable-auto-closing t
        web-mode-markup-indent-offset 4
        web-mode-css-indent-offset 2
        web-mode-code-indent-offset 2
        web-mode-enable-current-element-highlight t))

(use-package emmet-mode)
(use-package auto-rename-tag
  :hook (web-mode . auto-rename-tag-mode))

(use-package web-mode
  :hook
  (efs/web-mode-setup . web-mode)
  (emmet-mode . web-mode)
  (display-fill-column-indicator-mode . web-mode)
  :config
  (add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.css?\\'" . web-mode))
  (add-to-list 'auto-mode-alist '("\\.php?\\'" . web-mode)))

(use-package go-mode)

(use-package typescript-mode
  :hook
  (display-fill-column-indicator-mode . typescript-mode)
  (emmet-mode . typescript-mode)
  :config
  (add-to-list 'auto-mode-alist '("\\.ts?\\'" . typescript-mode))
  (add-to-list 'auto-mode-alist '("\\.js?\\'" . typescript-mode))
  (setq typescript-indent-level 4))

(defun compileandrun ()
    (interactive)
    (save-buffer)
    (let ((current-prefix-arg '(4)))
         (call-interactively 'compile))
    (other-window 1)
    ;; (efs/shrink-win 8)
    (end-of-buffer))

(add-hook 'c++-mode-hook
    (lambda () (local-set-key (kbd "<f5>") #'compileandrun)))
(add-hook 'csharp-mode-hook
    (lambda () (local-set-key (kbd "<f5>") #'compileandrun)))

(setq-default c-basic-offset 4)  ; 4 spaces for indentation

(use-package geiser-guile
  :config
  (setq geiser-repl-skip-version-check-p 't)
  (setq geiser-guile-binary "/usr/bin/guile")
  (setq geiser-active-implementations '(guile)))

(defun efs/scheme-repl ()
  (interactive)
  (evil-window-split)
  (geiser-guile)
  (efs/turn-off-completion)
  (efs/shrink-win 8)
  (other-window 1))

(defun efs/scheme-load-file ()
  (interactive)
  (geiser-load-file (buffer-file-name)))

(defun efs/scheme-send-definition-and-go ()
  (interactive)
  (geiser-eval-definition)
  (other-window 1)
  (end-of-buffer))

(defun efs/scheme-send-region-and-go ()
  (interactive "r")
  (geiser-eval-region)
  (other-window 1)
  (end-of-buffer))

(defun efs/markdown-setup ()
  "Markdown setup."
  (variable-pitch-mode 1))

(use-package markdown-mode
  :init
  (setq markdown-command "multimarkdown")
  :mode (("README\\.md\\'" . gfm-mode)
         ("\\.md\\'"       . markdown-mode)
         ("\\.markdown\\'" . markdown-mode))
  :config
  (setq markdown-enable-math t
        markdown-hide-markup t)
  :hook
  (efs/markdown-setup . markdown-mode))

(use-package nasm-mode
  :config
  (add-to-list 'auto-mode-alist '("\\.asm?\\'" . nasm-mode))
  (add-to-list 'auto-mode-alist '("\\.s?\\'" . nasm-mode)))

(defun efs/org-font-setup ()
  "Replace list hyphen with dot."
  (font-lock-add-keywords 'org-mode
        '(("^ *\\([-]\\) "
           (0 (prog1 ()
                (compose-region
                 (match-beginning 1) (match-end 1) "•"))))))

  ; adjust Org LaTeX font
  (setq org-format-latex-options
        (plist-put org-format-latex-options :scale 1.7))

  ; set font face for heading
  (dolist (face '((org-level-1 . 1.4)
                  (org-level-2 . 1.32)
                  (org-level-3 . 1.24)
                  (org-level-4 . 1.06)
                  (org-level-5 . 1.0)
                  (org-level-6 . 1.0)
                  (org-level-7 . 1.0)
                  (org-level-8 . 1.0)))
    (set-face-attribute (car face) nil
                        :font efs/sans-serif-font
                        :weight 'bold
                        :height (cdr face)))

  ; (set-face-attribute 'org-block nil    :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-table nil    :inherit 'fixed-pitch)
  (set-face-attribute 'org-formula nil  :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil     :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-table nil    :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil  :inherit 'fixed-pitch)
  (set-face-attribute 'line-number nil :inherit 'fixed-pitch)
  (set-face-attribute 'line-number-current-line nil :inherit 'fixed-pitch))

(use-package org-faces
  :ensure nil
  :custom-face
  (org-todo  ((nil (:weight bold))))
  (org-done  ((nil (:weight bold))))
  (org-table ((nil (:inherit fixed-pitch))))
  (org-block ((nil (:inherit fixed-pitch))))
  (org-code  ((nil (:inherit (shadow fixed-pitch))))))

(defun efs/org-mode-setup ()
  "Org settings."
  (org-indent-mode) ; Indent according to heading
  (variable-pitch-mode 1) ; Enable sans-serif
  (electric-pair-mode nil) ; Disable autopairs
  (setf (cdr (assoc 'file org-link-frame-setup)) 'find-file))

(defun org-convert-to-pdf ()
  (interactive)
  (defvar export-dir "exports")
  (defvar filename (file-name-base buffer-file-name))
  (unless (file-exists-p export-dir)
    (make-directory export-dir))
  (org-latex-export-to-pdf)
  (delete-file (concat export-dir "/" filename ".tex")))

(use-package org
  :pin org
  :commands (org-capture org-agenda)
  :hook
  (org-mode . efs/org-mode-setup)
  (org-mode . (lambda () (electric-indent-local-mode -1)))
  :bind-keymap
  ("<f5>" . org-latex-export-to-pdf)
  :config
  (setq org-ellipsis "…"
        org-pretty-entities nil
        org-insert-heading-respect-content t
        org-fold-catch-invisible-edits 'show-and-error
        org-log-done 'time
        org-startup-folded nil ; Fold all headings
        org-image-actual-width nil
        org-src-fontify-natively t
        org-src-tab-acts-natively t
        org-hide-emphasis-markers t  ; Hide markers for italic, bold, etc.
        org-directory efs/org-dir  ; Set default org dir
        org-export-with-tags nil
        org-startup-with-inline-images nil  ; Display all images when opening org
        org-edit-src-content-indentation 0
        org-src-preserve-indentation t
        org-auto-align-tags nil
        org-tags-column 0
        org-startup-with-inline-images nil
        org-startup-with-latex-preview nil)
  (efs/org-font-setup))

(add-hook 'org-mode-hook
          (lambda ()
            (face-remap-add-relative 'fixed-pitch :height 1.2)
            (face-remap-add-relative 'variable-pitch :height 1.3)))

(use-package org-appear
  :after org
  :hook (org-mode . org-appear-mode))

(use-package org-fragtog
  :after org
  :hook (org-mode . org-fragtog-mode))

(defun efs/org-modify-preview-dir ()
  (setq-local org-preview-latex-image-directory "~/.cache/emacs/ltximg/"))
(add-hook 'org-mode-hook 'efs/org-modify-preview-dir)

(use-package org-modern
  :after org
  :custom
  (org-modern-hide-stars nil)
  (org-modern-list '((?- . "•")))
  :hook
  (org-agenda-finalize . org-modern-agenda))

(use-package org-modern-indent
  :load-path "~/.config/emacs/repo/org-modern-indent/"
  :config
  (add-hook 'org-mode-hook #'org-modern-indent-mode 90))

(use-package olivetti
  :hook
  (org-mode . olivetti-mode))

(use-package toc-org
  :hook
  (org-mode . toc-org-mode)
  (markdown-mode . toc-org-mode)
  :config
  (define-key markdown-mode-map (kbd "\C-c\C-o") 'toc-org-markdown-follow-thing-at-point))

(defun org-syntax-table-modify ()
  "Modify `org-mode-syntax-table' for the current org buffer."
  (modify-syntax-entry ?< "." org-mode-syntax-table)
  (modify-syntax-entry ?> "." org-mode-syntax-table))

(add-hook 'org-mode-hook #'org-syntax-table-modify)

(use-package org-agenda
  :ensure nil
  :bind ("C-c a" . org-agenda)
  :custom
  (org-agenda-include-diary t)
  (org-habit-graph-column 80)
  (org-habit-today-glyph ?⧖)
  (org-habit-completed-glyph ?✓)
  (org-agenda-window-setup 'current-window)
  :config
  (setq org-agenda-files '("~/Documents/org/work.org"
                           "~/Documents/roam/inbox.org")
        org-agenda-tags-column 0
        org-agenda-time-grid
        '((daily today require-timed) (800 1000 1200 1400 1600 1800 2000) " ┄┄┄┄┄ " "┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄")
        org-agenda-current-time-string
        "◀── now ─────────────────────────────────────────────────"))

(use-package org-roam
  :bind (:map org-mode-map ("C-M-i" . completion-at-point))
  :custom
  (org-roam-directory (file-truename "~/Documents/roam"))
  (org-roam-completion-everywhere t)
  (org-id-link-to-org-use-id t)
  (org-roam-db-gc-threshold most-positive-fixnum)
  (org-roam-capture-templates
   '(
     ("i" "artificial-intelligence" plain "%?"
      :if-new
      (file+head "main/artificial-intelligence/${slug}.org"
                 "#+title: ${title}\n#+date: %U\n#+latex_header: \\usepackage{mathtools}\n#+filetags: :draft:note:ai:\n")
      :immediate-finish t
      :unnarrowed t)
     
     ("t" "general-tech" plain "%?"
      :if-new
      (file+head "main/general-tech/${slug}.org"
                 "#+title: ${title}\n#+date: %U\n#+latex_header: \\usepackage{mathtools}\n#+filetags: :draft:note:\n")
      :immediate-finish t
      :unnarrowed t)
     
     ("l" "linux-related" plain "%?"
      :if-new
      (file+head "main/linux-related/${slug}.org"
                 "#+title: ${title}\n#+date: %U\n#+latex_header: \\usepackage{mathtools}\n#+filetags: :draft:linux:\n")
      :immediate-finish t
      :unnarrowed t)
     
     ("n" "networking" plain "%?"
      :if-new
      (file+head "main/networking/${slug}.org"
                 "#+title: ${title}\n#+date: %U\n#+latex_header: \\usepackage{mathtools}\n#+filetags: :draft:networking:\n")
      :immediate-finish t
      :unnarrowed t)
     
     ("w" "web-dev" plain "%?"
      :if-new
      (file+head "main/programming/web-dev/${slug}.org"
                 "#+title: ${title}\n#+date: %U\n#+latex_header: \\usepackage{mathtools}\n#+filetags: :draft:web_dev:\n")
      :immediate-finish t
      :unnarrowed t)
     
     ("s" "lists" plain "%?"
      :if-new
      (file+head "main/lists/${slug}.org"
                 "#+title: ${title}\n#+date: %U\n#+latex_header: \\usepackage{mathtools}\n#+filetags: :draft:list:\n")
      :immediate-finish t
      :unnarrowed t)
     
     ("p" "programming" plain "%?"
      :if-new
      (file+head "main/programming/general/${slug}.org"
                 "#+title: ${title}\n#+date: %U\n#+latex_header: \\usepackage{mathtools}\n#+filetags: :draft:programming:\n")
      :immediate-finish t
      :unnarrowed t)

     ("a" "article" plain "%?"
      :if-new
      (file+head "article/${title}.org"
                 "#+title: ${title}\n#+filetags: :draft:article:\n")
      :immediate-finish t
      :unnarrowed t)

     ("r" "reference" plain "%?"
      :if-new
      (file+head "reference/${citar-citekey}.org"
                 "#+title: ${citar-title}\n#+author: ${citar-author}\n#+date: %U\n#+filetags: :draft:reference:")
      :immediate-finish t
      :unnarrowed t)))
  :config
  (org-roam-db-autosync-enable))

(setq org-roam-node-display-template
      (concat "${title}    " (propertize "[ ${tags} ]" 'face 'org-tag)))

; Using citar package
(use-package citar
  :custom
  (citar-bibliography '("~/Documents/roam/biblio.bib"))
  :hook
  (LaTeX-mode . citar-capf-setup)
  (org-mode . citar-capf-setup))

; And integrate with org-roam
(use-package citar-org-roam
  :after (citar org-roam)
  :config
  (citar-org-roam-mode))
(setq citar-org-roam-note-title-template "${title}")
(setq citar-org-roam-capture-template-key "r")

; Dependencies
(use-package simple-httpd)
(use-package websocket
  :after org-roam)
(use-package org-roam-ui
  :after org-roam
  :config
  (setq org-roam-ui-sync-theme t
        org-roam-ui-follow nil
        org-roam-ui-update-on-save t
        org-roam-open-on-start nil))

(setq org-capture-templates
      `(("s" "Slipbox" entry (file "~/Documents/roam/inbox.org")
         "* %?\n" :empty-lines 1)))
(defun efs/org-capture-slipbox ()
  (interactive)
  (org-capture nil "s"))

(with-eval-after-load 'org
  (global-org-modern-mode)
  (org-babel-do-load-languages
    'org-babel-load-languages
    '((emacs-lisp . t)
      (python . t)
      (dot . t)
      (plantuml . t)
      (latex . t)
      (C . t)
      (scheme . t)))
  (push '("conf-unix" . conf-unix) org-src-lang-modes))

(defun efs/org-confirm-babel-evaluate (lang body)
  "List of languages to enable org-babel."
  (not (member lang '("python" "C++" "C" "emacs-lisp" "dot" "plantuml" "latex" "scheme"))))
(setq org-confirm-babel-evaluate 'efs/org-confirm-babel-evaluate)

(add-hook 'org-babel-after-execute-hook 'org-redisplay-inline-images)

(with-eval-after-load 'org
  (require 'org-tempo)
  (add-to-list 'org-structure-template-alist '("sh" . "src shell"))
  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
  (add-to-list 'org-structure-template-alist '("do" . "src dot :file images/.png :exports results"))
  (add-to-list 'org-structure-template-alist '("scm" . "src scheme"))
  (add-to-list 'org-structure-template-alist '("cp" . "src C++ :includes <bits/stdc++.h> :results output"))
  (add-to-list 'org-structure-template-alist '("cl" . "src C :includes '(<stdio.h> <stdlib.h>) :results output"))
  (add-to-list 'org-structure-template-alist '("py" . "src python :results output"))
  (add-to-list 'org-structure-template-alist '("cf" . "src conf-space")))

(defun efs/org-babel-tangle-config ()
  (when (string-equal (file-name-directory (buffer-file-name))
                      (expand-file-name user-emacs-directory))
    (let ((org-confirm-babel-evaluate nil))
      (org-babel-tangle))))

(add-hook 'org-mode-hook (lambda () (add-hook 'after-save-hook #'efs/org-babel-tangle-config)))

(setq org-latex-pdf-process
      '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"
        "pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))

(with-eval-after-load 'ox-latex
  (add-to-list 'org-latex-classes
               '("my-article"
                 "\\documentclass[11pt,a4paper]{article}
\\usepackage[utf8]{inputenc}
\\usepackage[T1]{fontenc}
\\usepackage{fixltx2e}
\\usepackage{graphicx}
\\usepackage{longtable}
\\usepackage{float}
\\usepackage{listings}
\\usepackage{wrapfig}
\\usepackage{rotating}
\\usepackage[normalem]{ulem}
\\usepackage{amsmath}
\\usepackage{textcomp}
\\usepackage{marvosym}
\\usepackage{wasysym}
\\usepackage{amssymb}
\\usepackage{hyperref}
\\usepackage{mathpazo}
\\usepackage{color}
\\usepackage{enumerate}
\\tolerance=1000
      [NO-DEFAULT-PACKAGES]
      [PACKAGES]
      [EXTRA]
\\linespread{1.1}
\\hypersetup{pdfborder=0 0 0}"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")))
  )

(setq org-latex-listings 't)

(use-package pdf-tools
  :init
  (pdf-tools-install)
  :config
  (add-hook 'pdf-view-mode-hook (internal-show-cursor nil nil)))

(use-package tex
  :ensure auctex
  :after latex
  :defer t
  :init
  (setq tab-width 2
        LaTeX-item-indent 0
        latex "latex"
        pdf-latex-command "xelatex"
        TeX-view-program-selection '((output-pdf "PDF Tools"))
        TeX-view-program-list '(("PDF Tools" TeX-pdf-tools-sync-view)))
  (setq-default TeX-engine 'xetex)
  :hook
  (LaTeX-mode . (lambda ()
                  (add-to-list
                   'TeX-command-list
                   '("My-XeLaTeX" "%`xelatex -shell-escape --synctex=1%(mode)%' %t"
                     TeX-run-TeX nil t))
                  (setq TeX-command-default "XeLaTeX"
                        TeX-source-correlate-mode t)))
  (LaTeX-mode . efs/turn-on-visual-line)
  :config
  (progn
    (setq TeX-master "main.tex")
    (setq TeX-parse-self t)
    (setq TeX-auto-save t))
  :bind
  (:map LaTeX-mode-map
        ("C-l <backspace>" .
         (lambda ()
           (interactive)
           (TeX-clean)
           (message "Cleaned!")))))

(use-package company-auctex
  :init (company-auctex-init))

(setq tex-fontify-script nil)

(defun latex-compile ()
  "Compile macro for latex."
  (interactive)
  (save-buffer)
  (TeX-command "My-XeLaTeX" 'TeX-master-file))

(defun latex-compile-bib ()
  "Compile macro for latex and bibtex."
  (interactive)
  (save-buffer)
  (TeX-command "BibTeX" 'TeX-master-file))


(eval-after-load 'latex
  '(define-key LaTeX-mode-map (kbd "<f5>") 'latex-compile))
(eval-after-load 'latex
  '(define-key LaTeX-mode-map (kbd "<f6>") 'latex-compile-bib))

(use-package lsp-mode
  :init (setq lsp-keymap-prefix "C-c l")
  :hook
  (python-mode . lsp-deferred)
  (c++-mode . lsp-deferred)
  (c-mode . lsp-deferred)
  (typescript-mode . lsp-deferred)
  (js-mode . lsp-deferred)
  (web-mode . lsp-deferred)
  (lsp-lens-mode . lsp-deferred)
  :config
  (setq lsp-headerline-breadcrumb-enable nil
        lsp-enable-file-watchers nil
        lsp-inhibit-message t)
  (lsp-enable-which-key-integration t))

(use-package lsp-ui
  :hook (lsp-mode . lsp-ui-mode)
  :custom
  (lsp-ui-doc-position 'bottom)
  :config
  (setq lsp-ui-doc-frame-mode nil
        lsp-ui-sideline-show-hover nil
        lsp-ui-sideline-show-symbol nil
        lsp-ui-sideline-show-code-actions nil
        lsp-ui-sideline-show-diagnostics t
        lsp-ui-sideline-update-mode 'point))

(use-package lsp-treemacs
  :after lsp)

(use-package lsp-ivy :after lsp)

(defun efs/flycheck-lsp-diag ()
  "Macro for spawning error list."
  (interactive)
  (flycheck-list-errors)
  (other-window 1)
  (efs/shrink-win 8))

(use-package flycheck
  :hook
  (python-mode . flycheck-mode)
  (c-mode . flycheck-mode)
  (c++-mode . flycheck-mode)
  (js-mode . flycheck-mode)
  (typescript-mode . flycheck-mode)
  (tsx-ts-mode . flycheck-mode))

(use-package lsp-pyright
  :hook
  (python-mode . (lambda () (require 'lsp-pyright) (lsp))))

(use-package which-key
  :defer 0
  :diminish which-key-mode
  :config
  (which-key-mode)
  (setq which-key-idle-delay 1))

(use-package general
  :after evil
  :config
  (general-evil-setup)
  (general-create-definer efs/leader-keys
    :keymaps '(normal insert visual emacs)
    :prefix "SPC"
    :global-prefix "C-SPC"))

; ESC will exit prompts
(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

; Integrate more vim mode motions
(global-set-key (kbd "C-j") 'evil-window-down)
(global-set-key (kbd "C-k") 'evil-window-up)
(global-set-key (kbd "C-l") 'evil-window-right)
(global-set-key (kbd "C-h") 'evil-window-left)

; Disable secondary selection commands
(keymap-global-unset "M-<mouse-1>")
(keymap-global-unset "M-<mouse-2>")
(keymap-global-unset "M-<mouse-3>")
(keymap-global-unset "M-<drag-mouse-1>")
(keymap-global-unset "M-<down-mouse-1>")

(use-package evil-nerd-commenter
  :bind ("M-/" . evilnc-comment-or-uncomment-lines))

(efs/leader-keys

  "t" '(:ignore t :which-key "toggle+")
  "tt" '(counsel-load-theme :which-key "toggle-choose-theme")
  "tm" '(heaven-and-hell-toggle-theme :which-key "toggle-light-dark-theme")
  "td" '(disable-theme :which-key "toggle-default-theme")

  "b" '(:ignore t :which-key "buffer+")
  "bb" '(helm-buffers-list :which-key "buffer-switch")
  "bd" '(kill-buffer-and-window :which-key "buffer-delete")
  "bn" '(next-buffer :which-key "buffer-next")
  "bp" '(previous-buffer :which-key "buffer-prev")
  "bw" '(visual-line-mode :which-key "buffer-wrap-lines")

  "f" '(:ignore t :which-key "file+")
  "fv" '(eval-buffer :which-key "file-eval")
  "fs" '(save-buffer :which-key "file-save")
  "ff" '(find-file :which-key "file-find")
  "fn" '(dired-jump :which-key "file-find-project")
  "fg" '(projectile-ripgrep :which-key "file-grep-project")

  "fk" '(:ignore t :which-key "bookmark+")
  "fkm" '(bookmark-set :which-key "bookmark-current-file")
  "fks" '(bookmark-save :which-key "bookmark-save")
  "fkb" '(bookmark-jump :which-key "bookmark-jump")

  "o" '(:ignore t :which-key "org+")
  "ol" '(org-insert-link :which-key "org-insert-link")
  "oa" '(org-agenda :which-key "org-agenda")
  "og" '(org-babel-tangle :which-key "org-tangle")
  "ob" '(org-mark-ring-goto :which-key "org-go-back-prev-node")
  "oc" '(org-capture :which-key "org-capture")
  "of" '(org-fragtog-mode :which-key "org-fragtog-mode")
  "ot" '(counsel-imenu :which-key "show-contents")

  ; Displaying stuff
  "od"  '(:ignore t :which-key "org-display+")
  "odi" '(org-redisplay-inline-images :which-key "org-redisplay-images")
  "odl" '(org-latex-preview :which-key "org-display-latex-preview")

  ; Org Roam
  "or" '(:ignore t :which-key "org-roam+")
  "orf" '(org-roam-node-find :which-key "org-roam-find")
  "ori" '(org-roam-node-insert :which-key "org-roam-insert")
  "orc" '(citar-open-notes :which-key "org-roam-ref")
  "ors" '(org-roam-db-sync :which-key "org-roam-db-sync")

  "d" '(:ignore t :which-key "todo+")
  "dp" '(hl-todo-previous :which-key "todo-previous")
  "dn" '(hl-todo-next :which-key "todo-next")

  "v" '(:ignore t :which-key "lsp+")
  "vr" '(lsp-rename :which-key "lsp-rename-symbol")
  "vs" '(lsp-treemacs-symbols :which-key "lsp-show-symbols")
  "vl" '(efs/flycheck-lsp-diag :which-key "lsp-show-diagnostics")

  "g" '(:ignore t :which-key "git+")
  "gt" '(magit :which-key "git-status")

  "p" '(:ignore t :which-key "projectile+")
  "pp" '(projectile-switch-project :which-key "select-project")
  "pf" '(projectile-find-file :which-key "select-file-in-current-project")

  "m" '(:ignore t :which-key "music")
  "mb" '(emms-smart-browse :which-key "music-browse")
  "mu" '(emms-player-mpd-update-all-reset-cache :which-key "music-update-db")

  "sc" '(:ignore t :which-key "scheme")
  "scr" '(efs/scheme-repl :which-key "scheme-repl")
  "scf" '(efs/scheme-load-file :which-key "scheme-load-file")
  "scd" '(efs/scheme-send-definition-and-go :which-key "scheme-send-definition")
  "scg" '(efs/scheme-send-region-and-go :which-key "scheme-send-region")

  "u" '(:ignore t :which-key "browse")
  "uw" '(browse-web :which-key "browse-with-eww")
  "uf" '(browse-url-firefox :which-key "browse-with-firefox")
  "uc" '(browse-url-chrome :which-key "browse-with-chrome"))

(use-package pass :pin melpa)
;; Fix for GPG not prompting for passphrase
(setq epg-pinentry-mode 'loopback)

(use-package vterm)
(setq vterm-always-compile-module t)

(use-package magit
  :custom
  (magit-display-buffer-function
   #'magit-display-buffer-same-window-except-diff-v1))

(use-package emms
  :config
  (require 'emms-setup)
  (require 'emms-player-mpd)
  (emms-all)
  (setq emms-seek-seconds 5)
  (setq emms-info-functions '(emms-info-mpd))
  (setq emms-player-list '(emms-player-mpd))
  (setq emms-source-file-default-directory (expand-file-name "~/.config/mpd/playlists"))
  (setq emms-player-mpd-server-name "localhost")
  (setq emms-player-mpd-server-port "6600")
  (setq emms-player-mpd-music-directory "/ar1/Music")
  :hook
  (emms-playlist-cleared-hook . emms-player-mpd-clear))

(use-package elfeed
  :ensure t
  :defer t
  :init
  (setq elfeed-db-directory (expand-file-name "elfeed" user-emacs-directory)
        elfeed-search-filter "@6-months-ago +unread"))

(use-package elfeed-org
  :ensure t
  :init
  (setq rmh-elfeed-org-files (list "~/.config/emacs/Feed.org"))
  :config
  (elfeed-org))

(defun efs/elfeed-load-db-and-open ()
  (interactive)
  (elfeed-org)
  (elfeed-db-load)
  (elfeed)
  (elfeed-search-update--force))

(defun efs/elfeed-save-db-and-bury ()
  (interactive)
  (elfeed-db-save)
  (quit-window))

(global-set-key (kbd "C-x w") 'efs/elfeed-load-db-and-open)

; Fix garbage collection
(setq gc-cons-threshold 100000000)
(run-with-idle-timer 2 t (lambda () (garbage-collect)))

; Increase the amount of data which Emacs reads from the process
(setq read-process-output-max (* 1024 1024))

(setq lsp-idle-delay 0.500)
(setq lsp-log-io nil)

; fontifies the buffer when idle
(setq jit-lock-stealth-time 1.5)
(setq jit-lock-stealth-nice 0.5) ;; Seconds between font locking.
(setq jit-lock-chunk-size 4096)
;; avoid fontification while typing
(setq jit-lock-defer-time 0)
(with-eval-after-load 'evil
  (add-hook 'evil-insert-state-entry-hook
    (lambda ()
      (setq jit-lock-defer-time 0.25)) nil t)
  (add-hook 'evil-insert-state-exit-hook 
    (lambda ()
      (setq jit-lock-defer-time 0)) nil t))

(use-package mwheel
  :ensure nil
  :custom
  (mouse-wheel-scroll-amount '(1 ((shift) . 1)))
  (mouse-wheel-progressive-speed nil)
  (mouse-wheel-follow-mouse 't)
  :config
  (setq scroll-step 1)
  (setq scroll-conservatively 1000))

(use-package command-log-mode
  :commands command-log-mode)

(use-package helpful
  :commands (helpful-callable helpful-variable helpful-command helpful-key)
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

(setq native-comp-async-report-warnings-errors nil)

(use-package exec-path-from-shell
  :init
  (setq exec-path-from-shell-arguments nil)
  :config
  (exec-path-from-shell-initialize))

(setq browse-url-browser-function 'browse-url-firefox)
