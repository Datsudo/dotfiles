# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.config/zsh/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

PLUGINS="$ZDOTDIR/plugins"

# Import files
[ -f $XDG_CONFIG_HOME/aliasrc    ]; source $XDG_CONFIG_HOME/aliasrc
[ -f $XDG_CONFIG_HOME/functionrc ]; source $XDG_CONFIG_HOME/functionrc
[ -f $XDG_CONFIG_HOME/fzfrc      ]; source $XDG_CONFIG_HOME/fzfrc
[ -f $XDG_CONFIG_HOME/nnnrc      ]; source $XDG_CONFIG_HOME/nnnrc
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm

# -- Lines configured by zsh-newuser-install
HISTSIZE=100000
SAVEHIST=100000

# Change dir just by typing dirname
setopt autocd

# Turn off error beep
unsetopt beep

# Set Vi keymap
bindkey -v
# -- End of lines configured by zsh-newuser-install

# -- The following lines were added by compinstall
zstyle :compinstall filename '/home/datsudo/.config/zsh/.zshrc'
fpath+=~/.config/zsh/zfunc

autoload -Uz compinit
compinit

autoload -U zmv
# -- End of lines added by compinstall

# -- Set zcompdump to cache directory
compinit -d $XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION
zstyle ':completion:*' cache-path $XDG_CACHE_HOME/zsh/zcompcache

# -- Add plugins
[ -f $PLUGINS/dirhistory.zsh                                      ]; source $PLUGINS/dirhistory.zsh
[ -f $PLUGINS/zsh-autoenv/autoenv.zsh                             ]; source $PLUGINS/zsh-autoenv/autoenv.zsh
[ -f $PLUGINS/fzf-tab/fzf-tab.plugin.zsh                          ]; source $PLUGINS/fzf-tab/fzf-tab.plugin.zsh 
[ -f $PLUGINS/zsh-vi-mode/zsh-vi-mode.plugin.zsh                  ]; source $PLUGINS/zsh-vi-mode/zsh-vi-mode.plugin.zsh
[ -f $PLUGINS/powerlevel10k/powerlevel10k.zsh-theme               ]; source $PLUGINS/powerlevel10k/powerlevel10k.zsh-theme
[ -f $PLUGINS/zsh-autosuggestions/zsh-autosuggestions.zsh         ]; source $PLUGINS/zsh-autosuggestions/zsh-autosuggestions.zsh
[ -f $PLUGINS/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh ]; source $PLUGINS/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh


nnn_cd() {
    if ! [ -z "$NNN_PIPE" ]; then
        printf "%s\0" "0c${PWD}" > "${NNN_PIPE}" !&
    fi
}

trap nnn_cd EXIT

# [[ ! -r $HOME/.opam/opam-init/init.zsh ]] || source $HOME/.opam/opam-init/init.zsh  > /dev/null 2> /dev/null

# If the .xsession-errors file is not a symbolic link, delete it and create it as such
if [ ! -h $HOME/.xsession-errors ]; then
    /bin/rm $HOME/.xsession-errors
    ln -s /dev/null $HOME/.xsession-errors > /dev/null 2>&1
fi
if [ ! -h $HOME/.xsession-errors.old ]; then
    /bin/rm $HOME/.xsession-errors.old
    ln -s /dev/null $HOME/.xsession-errors.old > /dev/null 2>&1
fi

if [ ! -z $ZED ]; then
    export PATH="$PATH:$HOME/.local/bin:$HOME/go/bin:$HOME/.local/share/npm/bin:$HOME/.emacs.d/bin:$HOME/.local/share/cargo/bin:$HOME/.local/share/go/bin:$HOME/.local/share/gem/ruby/3.0.0/bin:$HOME/.config/emacs/bin:$HOME/.config/composer/vendor/bin:$HOME/Documents/dev/scripts"
fi

if [[ "$TERM" == "dumb" ]]; then
    unsetopt zle
    unsetopt prompt_cr
    unsetopt prompt_subst
    if whence -w precmd >/dev/null; then
        unfunction precmd
    fi
    if whence -w preexec >/dev/null; then
        unfunction preexec
    fi
    PS1="%B%n@%m%b %1~$ "
    return
fi

if [[ "$TERM_PROGRAM" == "vscode" && -f ".env-local" ]]; then
    source .env-local
fi

# eval "$(starship init zsh)"
eval "$(zoxide init --cmd cd zsh)"
eval "$(pyenv init -)"
eval "$(direnv hook zsh)"

# To customize prompt, run `p10k configure` or edit ~/.config/zsh/.p10k.zsh.
[[ ! -f ~/.config/zsh/.p10k.zsh ]] || source ~/.config/zsh/.p10k.zsh
