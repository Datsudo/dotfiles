#!/bin/sh

killall waybar

CONFIG_PATH=~/.dotfiles/.config/waybar

if [[ $USER = "datsudo" ]]
then
    waybar -c $CONFIG_PATH/config.jsonc & -s $CONFIG_PATH/style.css
else
    waybar &>/dev/null
fi