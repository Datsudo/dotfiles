# .bashrc

# Source global definitions
[ -f /etc/bashrc ] && . /etc/bashrc

[ -f ~/.config/aliasrc ] && . ~/.config/aliasrc
[ -f ~/.config/functionrc ] && . ~/.config/functionrc
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"
[ -s "NVM_DIR/bash_completion" ] && . "$NVM_DIR/bash_completion"

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$PATH"
fi
export PATH


unset rc
