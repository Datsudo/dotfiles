export ZDOTDIR="$HOME/.config/zsh"

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_STATE_HOME="$HOME/.local/state"

export WGETRC="$XDG_CONFIG_HOME/wgetrc"
export RIPGREP_CONFIG_PATH="$XDG_CONFIG_HOME/ripgrep/config"
export STARSHIP_CONFIG="$XDG_CONFIG_HOME/starship.toml"
export STARSHIP_CACHE="$XDG_CACHE_HOME/starship"
export HISTFILE="$XDG_STATE_HOME/zsh/history"
export SQLITE_HISTORY="$XDG_CACHE_HOME/sqlite_history"
export NODE_REPL_HISTORY="$XDG_DATA_HOME"/node_repl_history
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
export ICEAUTHORITY="$XDG_CACHE_HOME/ICEauthority"
export PYTHON_HISTORY="$XDG_STATE_HOME/python/history"
export INPUTRC="$XDG_CONFIG_HOME"/readline/inputrc

export PYENV_ROOT="$XDG_DATA_HOME/pyenv"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export PYLINTHOME="$XDG_CACHE_HOME/pylint"
export NPM_HOME="$XDG_DATA_HOME/npm"
export PARALLEL_HOME="$XDG_CONFIG_HOME"/parallel
export JUPYTER_CONFIG_DIR="$XDG_CONFIG_HOME/jupyter"
export DOCKER_CONFIG="$XDG_CONFIG_HOME/docker"
export PYTHONPYCACHEPREFIX="$XDG_CACHE_HOME/python"
export PYTHONUSERBASE="$XDG_DATA_HOME/python"

export PASSWORD_STORE_DIR="$XDG_DATA_HOME/pass"
export W3M_DIR="$XDG_STATE_HOME/w3m"
export FFMPEG_DATADIR="$XDG_CONFIG_HOME/ffmpeg"
export IPYTHONDIR="$XDG_CONFIG_HOME/ipython"
export GOPATH="$XDG_DATA_HOME/go"
export GOBIN="$XDG_DATA_HOME/go/bin"
export NVM_DIR="$XDG_CONFIG_HOME/nvm"

export SVDIR="$HOME/.config/service"

export TEXMFHOME="$XDG_DATA_HOME/texmf"
export TEXMFVAR="$XDG_CACHE_HOME/texlive/texmf-var"
export TEXMFCONFIG="$XDG_CONFIG_HOME/texlive/texmf-config"

# export QT_QPA_PLATFORMTHEME="gtk2"

export PYDEVD_DISABLE_FILE_VALIDATION=1

export NO_AT_BRIDGE=1

export EDITOR="emacsclient -c"
export VISUAL="$EDITOR"

export LESS=-R

export LS_COLORS=':ow=01;33'

export HISTIGNORE='*sudo -S*'
export HISTORY_IGNORE='(*sudo -S*|ls|ll)'

export BOOKS="/ar1/Library/Readings"
export DRIVE1="/ar1"
export UNI="/ar1/Library/Uni"
export COURSES="/ar1/Courses"
export DEV_DIR="$HOME/Documents/dev"
export DOCS_DIR="$HOME/Documents"
export MUSIC_DIR="/ar1/Music"
export DEVLAB="$DEVDIR/lab"
export WIN10="/win/Users/Datsudo"
export DOTFILES="$HOME/.dotfiles"
export USER_BIN="$HOME/.local/bin"

export GOOGLE_API_KEY="no"
export GOOGLE_DEFAULT_CLIENT_ID="no"
export GOOGLE_DEFAULT_CLIENT_SECRET="no"

# # Wayland-specific shit
# export QT_QPA_PLATFORM=wayland-egl
# export SDL_VIDEODRIVER=wayland
# export MOZ_ENABLE_WAYLAND=1
# export QT_WAYLAND_DISABLE_WINDOWDECORATION="1"

export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"


export PATH="$PATH:$HOME/.local/bin:$HOME/go/bin:$HOME/.local/share/npm/bin:$HOME/.emacs.d/bin:$HOME/.local/share/cargo/bin:$HOME/.local/share/go/bin:$HOME/.local/share/gem/ruby/3.0.0/bin:$HOME/.config/composer/vendor/bin:$HOME/Documents/dev/scripts:$HOME/.local/share/pyenv/bin:$HOME/.nimble/bin"
